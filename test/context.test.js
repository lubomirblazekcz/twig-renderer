const TwigRenderer = require('../twig-renderer');

describe('Context hierarchy', () => {
    test('context from template.json file', () => {
        const twigRenderer = new TwigRenderer({
            globals: 'test/fixtures/globals.json',
            context: {
                value: 'value from options.context'
            }
        });

        return twigRenderer.render('test/fixtures/page-1.twig').then((html) => {
            expect(html.toString().trim()).toBe('value from page json file');
        });
    });

    test('context from globals', () => {
        const twigRenderer = new TwigRenderer({
            globals: 'test/fixtures/globals.json',
            context: {
                value: 'value from options.context'
            }
        });

        return twigRenderer.render('test/fixtures/page-2.twig').then((html) => {
            expect(html.toString().trim()).toBe('value from options.global');
        });
    });

    test('context from options', () => {
        const twigRenderer = new TwigRenderer({
            context: {
                value: 'value from options.context'
            }
        });

        return twigRenderer.render('test/fixtures/page-2.twig').then((html) => {
            expect(html.toString().trim()).toBe('value from options.context');
        });
    });
});
